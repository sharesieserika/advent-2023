import re


def get_top_three():
    with open("./input/1_1.txt") as file:
        totals = []
        for line in file.readlines():
            if line.strip():
                print(line)
                first = get_first_number(line)
                last = get_last_number(line) or first
                totals.append(int(first + last))
                print(first, last)

        print(sum(totals))

def get_first_number(string):
    regex = re.compile(r"(?P<word>\d|one|two|three|four|five|six|seven|eight|nine)")
    matches = regex.findall(string)
    if matches:
        return str(string_to_int(matches[0]))
    return None

def get_last_number(string):
    for i in range(len(string) - 1, 0, -1):
        if string[i].isdigit():
            return string[i]

        if first_number := get_first_number(string[i:]):
            return first_number

def string_to_int(string):
    try:
        return int(string)
    except:
        if string == "one":
            return 1
        elif string == "two":
            return 2
        elif string == "three":
            return 3
        elif string == "four":
            return 4
        elif string == "five":
            return 5
        elif string == "six":
            return 6
        elif string == "seven":
            return 7
        elif string == "eight":
            return 8
        elif string == "nine":
            return 9


get_top_three()