
def seeds_for_part_2(string):
    ints = [int(n) for n in string.split(":")[1].split()]

    seeds = []
    for i in range(0, len(ints), 2):
        start = ints[i]
        end = start + ints[i + 1] - 1
        seeds.append(Range(start, end))

    return sorted(seeds, key=lambda x: x.start)


def build_conversions(lines):
    map = {}
    conversion_key = None
    transformations = []
    for line in lines:
        if not line.strip():
            continue

        if ':' in line:
            if conversion_key:
                map[conversion_key] = Conversion(transformations)
                transformations = []
            conversion_key = tuple(line.strip(' map:\n').split("-to-"))
        else:
            transformations.append(Transformation.from_string(line))

    map[conversion_key] = Conversion(transformations)
    return map


class Range:
    start: int
    end: int

    def __init__(self, start, end):
        self.start = start
        self.end = end

    def overlap(self, other):
        start = max(self.start, other.start)
        end = min(self.end, other.end)
        if start <= end:
            return Range(start, end)
        return None


class Transformation:
    source_range: Range
    destination_range: Range
    length: int
    delta: int

    @classmethod
    def from_string(cls, string):
        destination_start, source_start, length = [int(n) for n in string.split()]
        transformation = cls()
        transformation.length = length
        transformation.source_range = Range(source_start, source_start + length - 1)
        transformation.destination_range = Range(destination_start, destination_start + length - 1)
        transformation.delta = destination_start - source_start
        return transformation

    @classmethod
    def from_destination_range_and_delta(cls, destination_range, delta):
        transformation = cls()
        transformation.length = destination_range.end - destination_range.start + 1
        transformation.source_range = Range(destination_range.start - delta, destination_range.end - delta)
        transformation.destination_range = destination_range
        transformation.delta = delta
        return transformation


class Conversion:
    transformations_by_source: [(int, int, int)] = []
    transformations_by_destination: [(int, int, int)] = []

    def __init__(self, ranges: list[Transformation]):
        ranges = self.fill_gaps(ranges)
        self.transformations_by_source = sorted(ranges, key=lambda x: x.source_range.start)
        self.transformations_by_destination = sorted(ranges, key=lambda x: x.destination_range.start)

    def get_transformations_for_range(self, destination_range):
        transformations = []
        for transformation in self.transformations_by_source:
            if destination_overlap := transformation.destination_range.overlap(destination_range):
                transformations.append(
                    Transformation.from_destination_range_and_delta(destination_overlap, transformation.delta))
        return transformations

    @staticmethod
    def fill_gaps(transformations):
        """Takes a list of transformations, and inserts extra transformations to fill the gaps between them"""
        transformations.sort(key=lambda x: x.source_range.start)
        new_transformations = []
        current = Transformation.from_string("0 0 1")
        transformations.append(Transformation.from_string("100000000000 100000000000 1"))
        next = transformations[0]

        for i in range(len(transformations)):
            gap_length = next.source_range.start - (current.source_range.end + 1)
            if gap_length > 0:
                gap_start = current.source_range.end + 1
                new_transformations.append(Transformation.from_string(f"{gap_start} {gap_start} {gap_length}"))
            current = transformations[i]
            if i < len(transformations) - 1:
                new_transformations.append(current)
                next = transformations[i + 1]

        return new_transformations

    def source_to_destination(self, source):
        for transformation in self.transformations_by_source:
            source_start = transformation.source_range.start
            source_end = transformation.source_range.end
            if source_start <= source <= source_end:
                return source + transformation.delta
        return source


def find_lowest_possible_location(seed_ranges, conversion_map):
    # Depth-first traversal of the conversions in reverse.
    # At each new conversion, find all transformations that would convert the source to a destination within the
    # previous conversion's source range.
    # Follow all the way up the conversions and transformations to seed, then check if we have a seed within the range.
    order = ['seed', 'soil', 'fertilizer', 'water', 'light', 'temperature', 'humidity', 'location']
    conversion_steps = [(order[i - 1], order[i]) for i in range(len(order) - 1, 0, -1)]

    stack = [(-1, Transformation.from_string("0 0 100000000000"))]  # (step, transformation)
    end_step = len(conversion_steps) - 1
    smallest_seeds = []
    while stack:
        step, transformation = stack.pop()
        if step == end_step:
            if any([seed_range.overlap(transformation.source_range) for seed_range in seed_ranges]):
                overlapping_ranges = [seed_range.overlap(transformation.source_range) for seed_range in seed_ranges]
                smallest_seed = min([range.start for range in overlapping_ranges if range])
                smallest_seeds.append(smallest_seed)

        else:
            next_conversion = conversion_steps[step + 1]
            transformations = conversion_map[next_conversion].get_transformations_for_range(transformation.source_range)
            transformations_by_highest_destination = sorted(transformations, key=lambda x: -x.destination_range.start)
            stack += [(step + 1, transformation) for transformation in transformations_by_highest_destination]

    locations = []
    for source in smallest_seeds:
        for i in range(0, len(order) - 1):
            destination_name, source_name = order[i], order[i + 1]
            conversion = conversion_map[(destination_name, source_name)]
            source = conversion.source_to_destination(source)
        locations.append(source)
    print(min(locations))


def part_2():
    with open("input/5_1.txt") as file:
        lines = file.readlines()
        seed_ranges = seeds_for_part_2(lines[0])
        conversion_map = build_conversions(lines[2:])

    find_lowest_possible_location(seed_ranges, conversion_map)


part_2()
