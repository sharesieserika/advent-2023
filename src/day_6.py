def parse_input():
    with open("input/6_1.txt") as file:
        times_str, distances_str = file.readlines()
        times = [int(n) for n in times_str.split(':')[1].split()]
        distances = [int(n) for n in distances_str.split(':')[1].split()]
        return times, distances

def get_distance(total_seconds, acceleration_seconds):
    return acceleration_seconds * (total_seconds - acceleration_seconds)
def get_n_ways_to_win(total_seconds, record_distance):
    first_winner, last_winner = None, None
    mid_point = total_seconds // 2

    l_max = 0
    r_max = mid_point
    n_seconds = (r_max - l_max) // 2
    while not first_winner:
        distance = get_distance(total_seconds, n_seconds)
        distance_left = get_distance(total_seconds, n_seconds - 1)
        if distance >= record_distance:
            if distance_left <= record_distance:
                first_winner = n_seconds
            else:
                r_max = n_seconds
                n_seconds = l_max + (r_max - l_max) // 2
        else:
            l_max = n_seconds
            n_seconds = l_max + (r_max - l_max) // 2

        if n_seconds < 0 or n_seconds >= total_seconds:
            break

    if first_winner:
        last_winner = total_seconds - first_winner
        return last_winner - first_winner + 1
    return 0

def part_1():
    times, distances = parse_input()
    results = []
    for i in range(len(times)):
        time = times[i]
        distance = distances[i]
        results.append(get_n_ways_to_win(time, distance))
    print(results)
    x = 1
    for result in results:
        x *= result
    print(x)

part_1()
