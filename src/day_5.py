# seeds: 79 14 55 13
#
# seed-to-soil map:
# 50 98 2
# 52 50 48
from typing import NamedTuple


def seeds_for_part_1(string):
    return [int(n) for n in string.split(":")[1].split()]

def part_1():
    with open("input/5_0.txt") as file:
        lines = file.readlines()
        seeds = seeds_for_part_1(lines[0])
        i = 2
        maps = {}
        important_numbers = []
        conversion_key = ()
        while i < len(lines):
            if not lines[i].strip():
                i += 1
                continue
            line = lines[i]
            if ':' in line:
                if conversion_key:
                    maps[conversion_key].sort_ranges()
                conversion_key = tuple(line.strip(' map:\n').split("-to-"))
                conversion = Conversion([])
                maps[conversion_key] = conversion
            else:
                source_start, destination_start, length = [int(n) for n in line.split()]
                maps[conversion_key].add_range(source_start, destination_start, length)
                important_numbers.append(source_start)
                important_numbers.append(source_start + length)
            i += 1

        maps[conversion_key].sort_ranges()
        locations = []
        for seed in seeds[:]:
            print(f"starting seed {seed}")
            source = seed
            conversions = ['seed', 'soil', 'fertilizer', 'water', 'light', 'temperature', 'humidity', 'location']
            for i in range(0, len(conversions) - 1):
                destination_name, source_name = conversions[i], conversions[i + 1]
                conversion = maps[(destination_name, source_name)]
                source = conversion.source_to_destination(source)
            locations.append(source)

        print(min(locations))


class Conversion():
    ranges: [(int, int, int)] = []
    ranges_by_source: [(int, int, int)] = []
    ranges_by_destination: [(int, int, int)] = []
    def __init__(self, ranges):
        self.ranges = ranges

    def add_range(self, source_start, destination_start, length):
        self.ranges.append((source_start, destination_start, length))

    def source_to_destination(self, source):
        for destination_start, source_start, length in self.ranges:
            if source_start <= source < source_start + length:
                return source + (destination_start - source_start)
        return source

    def sort_ranges(self):
        self.ranges.sort(key=lambda x: x[0])
        self.ranges_by_source = self.ranges
        self.ranges_by_destination = sorted(self.ranges.copy(), key=lambda x: x[1])

def build_conversion(lines):
    i = 2
    maps = {}
    conversion_key = ()
    while i < len(lines):
        if not lines[i].strip():
            i += 1
            continue
        line = lines[i]
        if ':' in line:
            if conversion_key:
                maps[conversion_key].sort_ranges()
            conversion_key = tuple(line.strip(' map:\n').split("-to-"))
            conversion = Conversion([])
            maps[conversion_key] = conversion
        else:
            source_start, destination_start, length = [int(n) for n in line.split()]
            maps[conversion_key].add_range(source_start, destination_start, length)
        i += 1

    maps[conversion_key].sort_ranges()

    conversions = ['seed', 'soil', 'fertilizer', 'water', 'light', 'temperature', 'humidity', 'location']
    destination_name, source_name = conversions[0], conversions[1]
    conversion = maps[(destination_name, source_name)]
    for i in range(1, len(conversions) - 1):
        conversion += maps[(conversions[1], conversions[i + 1])]

    return conversion



part_1()
