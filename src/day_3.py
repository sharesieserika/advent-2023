import string

def build_graph(file):
    graph = dict()
    y_max = 0
    x_max = 0
    for y, line in enumerate(file.readlines()):
        y_max = max(y_max, y)
        for x, c in enumerate(line.strip()):
            x_max = max(x_max, x)
            graph[complex(x, y)] = c
    return graph, y_max, x_max

def part_1():
    with open("input/3_1.txt") as file:
        graph, y_max, x_max = build_graph(file)

        part_numbers = []
        y = 0
        while y <= y_max:
            x = 0
            number_string = ""
            is_touching_symbol = False

            while x <= x_max:
                pos = complex(x, y)
                char = graph.get(pos, '.')
                if char.isdigit():
                    number_string += char
                    is_touching_symbol = is_touching_symbol or neighbour_is_symbol(pos, graph)
                elif len(number_string) > 0:
                    if is_touching_symbol:
                        part_numbers.append(int(number_string))
                    number_string = ""
                    is_touching_symbol = False

                x += 1
            y += 1
        print(sum(part_numbers))

def neighbour_is_symbol(pos, graph):
    for direction in (-1, 1, -1j, 1j, -1 - 1j, -1 + 1j, 1 - 1j, 1 + 1j):
        neighbour_pos = pos + direction
        char = graph.get(neighbour_pos, '.')
        if not char.isdigit() and char != '.':
            return True
    return False


def part_2():
    with open("input/3_1.txt") as file:
        graph, y_max, x_max = build_graph(file)
        gear_pairs = []
        y = 0
        while y <= y_max:
            x = 0
            while x <= x_max:
                pos = complex(x, y)
                char = graph.get(pos, '.')
                if char == '*':
                    pairs = find_gear_pair(pos, graph)
                    if len(pairs) == 2:
                        gear_pairs.append(pairs)

                x += 1
            y += 1
        print(sum([pair[0] * pair[1] for pair in gear_pairs]))

def find_gear_pair(pos, graph):
    gears = []
    directions_clockwise = [-1 - 1j, -1j, 1 - 1j, 1, 1 + 1j, 1j, -1 + 1j, -1]
    d_i = 0
    while d_i < len(directions_clockwise) and len(gears) < 2:
        direction = directions_clockwise[d_i]
        neighbour = pos + direction
        char = graph.get(neighbour, '.')
        if char.isdigit():
            x_offset = 1
            number_string = char
            while (char := graph.get(neighbour - x_offset, '.')) and char.isdigit():
                number_string = char + number_string

                if direction - x_offset in directions_clockwise:
                    directions_clockwise.remove(direction - x_offset)
                x_offset += 1

            x_offset = 1
            while (char := graph.get(neighbour + x_offset, '.')) and char.isdigit():
                number_string += char

                if direction + x_offset in directions_clockwise:
                    directions_clockwise.remove(direction + x_offset)
                x_offset += 1

            gears.append(int(number_string))
        d_i += 1
    return gears

# part_1()
part_2()
