import math
import string
from typing import NamedTuple


def part_1():
    with open("input/4_1.txt") as file:
        cards = []
        # Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53

        for line in file.readlines():
            card, numbers = line.strip().split(": ")
            winners, yours = numbers.split(" | ")
            winners = set([int(n) for n in winners.strip().split(" ") if n])
            yours = set([int(n) for n in yours.strip().split(" ") if n])
            cards.append((card, winners, yours))
        results = []

        for card, winners, yours in cards:
            winning_numbers = winners.intersection(yours)
            results.append(winning_numbers)

        sum = 0
        for result in results:
            if len(result) > 0:
                sum += (math.pow(2, len(result) - 1))
        print(sum)

class Card():
    winners: set[int]
    yours: set[int]
    n_winners: int = 0
    n_copies: int = 1
    def __init__(self, winners, yours):
        self.winners = winners
        self.yours = yours
        self.n_winners = len(set.intersection(self.winners, self.yours))

def part_2():
    with open("input/4_1.txt") as file:
        cards = []

        for line in file.readlines():
            card, numbers = line.strip().split(": ")
            winners, yours = numbers.split(" | ")
            winners = set([int(n) for n in winners.strip().split(" ") if n])
            yours = set([int(n) for n in yours.strip().split(" ") if n])
            cards.append(Card(winners, yours))

        index = 0
        while index < len(cards):
            card = cards[index]
            for i in range(index + 1, index + card.n_winners + 1):
                cards[i].n_copies += card.n_copies

            index += 1

        sum = 0
        for card in cards:
            sum += card.n_copies
        print(sum)

part_1()
part_2()
