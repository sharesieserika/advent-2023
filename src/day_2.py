import math


def part_1():
    with open("input/2_1.txt") as file:
        totals = []
        games = []

        for line in file.readlines():
            #Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
            game_id, results = line.strip().split(": ")
            game_id = int(game_id.split(" ")[1])
            results = results.split("; ")
            pulls = []
            for result in results:
                colour_to_count = {}
                for colour in result.split(", "):
                    count, colour = colour.split(" ")
                    colour_to_count[colour] = int(count)
                pulls.append(colour_to_count)

            games.append([game_id, pulls])

        check = {
            "red": 12,
            "green": 13,
            "blue": 14
        }

        good_ids = []
        for game in games:
            game_id, pulls = game
            if pulls_are_good(pulls, check):
                good_ids.append(game_id)

        return sum(good_ids)

def pulls_are_good(pulls, check):
    for pull in pulls:
        if pull.get("red", 0) > check["red"] or pull.get("green",0) > check["green"] or pull.get("blue", 0) > check["blue"]:
            return False

        if any([key not in check for key in pull.keys()]):
            return False
    return True



def part_2():
    with open("input/2_1.txt") as file:
        games = []

        for line in file.readlines():
            #Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
            game_id, results = line.strip().split(": ")
            game_id = int(game_id.split(" ")[1])
            results = results.split("; ")
            pulls = []
            for result in results:
                colour_to_count = {}
                for colour in result.split(", "):
                    count, colour = colour.split(" ")
                    colour_to_count[colour] = int(count)
                pulls.append(colour_to_count)

            games.append([game_id, pulls])
        results = []
        for game in games:
            cube_mins = {}

            game_id, pulls = game
            for pull in pulls:
                for colour, count in pull.items():
                    if colour not in cube_mins:
                        cube_mins[colour] = count
                    else:
                        cube_mins[colour] = max(cube_mins.get(colour, 0), count)

            cube_min_ints = cube_mins.values()
            results.append(math.prod(cube_min_ints))
        return sum(results)

print(part_2())